import React, { Component } from 'react';
import {
    YellowBox,
    View,
    Text,
    Image,
    Alert,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    TextInput,
    StyleSheet
} from 'react-native';

YellowBox.ignoreWarnings(['Could not find image', 'Task orphaned', 'source.uri', 'Encountered an error']);

class App extends Component{
    constructor(props){
        super(props)
        this.state = {
            splashShow: true,
            since: 0,
            username: '',
            Users: [],
            isEndData: true,
            isLoading: true
        }
    }

    componentDidMount() {
        const { since } = this.state;
        this._fetchDataUser()
    }

    _fetchDataUser = async () => {
        fetch( 'https://api.github.com/users?since='+ this.state.since + '&per_page=30', {
            method: 'GET'
        }).then((response) => response.json())
        .then((responseJson) => {
            this.setState( state => ({Users: [...state.Users, ...responseJson]}))
        }).catch((error) => {
            Alert.alert('Ooppss', 'Something problem')
        });
    }

    handleEnd = () => {
        if (!this.state.isEndData) {
            this.setState(state => ({ since: this.state.since + 31 }), () => this._fetchDataUser())
            this.setState({isEndData : true})
        }
    }

    renderFooter = () => {
        if (!this.state.isLoading) return null;
        return (
            <View style={{ paddingVertical: 10 }}>
                <ActivityIndicator animating size="small" color='blue' hidesWhenStopped />
            </View>
        );
    };

    _findUser = async () => {
        if (this.state.username == ''){
            this.setState({ Users: [] })
            this._fetchDataUser()
        } else {
            this.setState({ isEndData: true })
            fetch( 'https://api.github.com/users/'+ this.state.username, {
                method: 'GET'
            }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.message == 'Not Found'){
                    this.setState({ Users: [], isLoading: false })
                } else {
                    this.setState({ Users: [responseJson], isLoading: false })
                }
            }).catch((error) => {
                Alert.alert('Ooppss', 'Something problem')
            });
        }
    }

    _listEmptyComponent = () => {
        if (!this.state.isLoading){
            return(
                <View style={styles.viewInfo}>
                    <Text style={{fontSize: 12, color: 'grey'}}>Not Found</Text>
                </View>
            )
        }
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.containerSearch}>
                    <TextInput
                        autoFocus
                        style={styles.inputStyle}
                        placeholder="Type Here"
                        onChangeText={(username) => this.setState({username})}
                        autoCapitalize= 'none'
                        autoCorrect= {false}
                        onSubmitEditing={() => this._findUser()}
                    />
                    <TouchableOpacity
                        onPress={() => this._findUser()}
                        style={styles.buttonCustom}>
                        <Text style={{fontSize: 16, color: '#fff'}}>Find</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    ref="flatlistUser"
                    extraData={this.state}
                    data={this.state.Users}
                    ListEmptyComponent={this._listEmptyComponent()}
                    onEndReached={() => this.handleEnd()}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={this.renderFooter}
                    onMomentumScrollBegin={() => this.setState({isEndData : false})}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity
                            onPress={()=> Alert.alert('Say Hi to, ' + item.login)}
                            style={styles.itemContainer}>
                            <Image source={{uri: item.avatar_url }} style={styles.itemImage}/>
                            <View>
                                <Text style={{fontSize: 16, color: '#000'}}>{item.login}</Text>
                                <Text style={{fontSize: 10, color: '#0080ff'}}>{item.html_url}</Text>
                            </View>
                            <Image source={require('./ic_arrow_blue.png')} style={styles.icon}/>
                        </TouchableOpacity>
                    }
                    keyExtractor={(item, index) => index.toString()} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },

    containerSearch: {
        flexDirection: 'row', justifyContent: 'space-between', padding: 10
    },

    inputStyle: {
        flex: 1, borderWidth: 1, marginRight: 5, padding: 5, paddingHorizontal: 10, borderRadius: 10, borderColor: 'grey'
    },

    buttonCustom: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#0080ff', borderRadius: 10, paddingHorizontal: 10
    },

    itemContainer: {
        flexDirection: 'row', marginVertical: 4, marginHorizontal: 10, alignItems: 'center', backgroundColor: '#fff', borderRadius: 10, padding: 5, elevation: 1
    },

    itemImage: {
        width: 50, height: 50, marginRight: 10, borderRadius: 8
    },

    icon: {
        width: 20, height: 20, resizeMode: 'contain', position: 'absolute', right: 10
    },

    viewInfo: {
        marginTop: 20, justifyContent: 'center', alignItems: 'center'
    }

})

export default App;